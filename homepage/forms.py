from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(StatusForm, self).__init__(*args, **kwargs)

                                                      
        self.fields['status'].error_messages = {'max_length': 'Batas maksimal karakter 300',
                                                'required': 'Status harus diisi'}


    class Meta:
        model = Status
        fields = ['status']
        
    
        

