from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse, resolve

from .forms import StatusForm
from .models import Status
from .views import home

from django.test import LiveServerTestCase
from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

import time



#Testing HTML
class FormHTMLTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.home_url = reverse('homepage:home')
        
    def test_text_exist(self):
        response = self.client.get(self.home_url)
        self.assertIn('Halo, Apa Kabar?', response.content.decode())

    def test_submit_button(self):
        response = self.client.get(self.home_url)
        self.assertContains(response,'Masukkan Statusmu')

    def test_form(self):
        response = self.client.get(self.home_url)
        self.assertContains(response, '<form')
        self.assertContains(response, '<button')
        self.assertContains(response, '<input')

        

# Testing URLs
class TestUrls(SimpleTestCase):
    def test_home_url_is_resolved(self):
        url = reverse('homepage:home')
        self.assertEquals(resolve(url).func, home)
    

    
# Testing Views
class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.home_url = reverse('homepage:home')
        Status.objects.create(
            status = 'depressed')
    
    def test_status_GET(self):
        response = self.client.get(self.home_url)
        
        self.assertEquals(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)
        self.assertTemplateUsed(response, 'home.html', 'base.html')
        
    def test_status_GET_wrong_url(self):
        url = '/kwek'
        response = self.client.get(url)
        
        self.assertEqual(response.status_code, 404)
        

    def test_status_POST(self):
        response = self.client.post(self.home_url,
                                   {'status': 'depresed'})
        
        self.assertEquals(response.status_code, 302)
        self.assertEquals(Status.objects.first().status, 'depressed')
        

        
# Testing Models
class TestModels(TestCase):
    def test_status(self):
        status1 = Status.objects.create(status = 'depressed')
        
        self.assertEquals(status1.status, 'depressed')
        
        
        
# Testing Forms
class TestForms(SimpleTestCase):
    def test_status_form_valid_data(self):
        form = StatusForm(data={'status': 'depressed'})
        self.assertTrue(form.is_valid())
    
    def test_status_form_no_data(self):
        form = StatusForm(data={})
        
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)
    

        
# Funcional Testing
class TestSelenium(StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')

    
    def tearDown(self):
        self.selenium.close()
        
    def test_user_sees_table(self):
        self.selenium.get(self.live_server_url + '/story_enam/')
        
        self.assertEquals(
            self.selenium.find_element_by_tag_name('h1').text, 'Halo, Apa Kabar?'
        )
        
        self.assertEquals(
            self.selenium.find_element_by_tag_name('label').text, 'Bagaimana Statusmu?'
        )
        
        self.assertEquals(
            self.selenium.find_element_by_id('button-submit').text, 'Masukkan Statusmu'
        )
        
        
        
    def test_user_sees_status(self):
        self.selenium.get(self.live_server_url + '/story_enam/')
        time.sleep(2)
        
        status = self.selenium.find_element_by_name('status')
        button = self.selenium.find_element_by_id('button-submit')

        status.send_keys('Coba Coba')
        time.sleep(2)
        button.click()
        time.sleep(2)

        self.assertInHTML('Coba Coba', self.selenium.page_source)
        
    
    
    def test_user_is_redirected(self):
        self.selenium.get(self.live_server_url + '/story_enam/')
        
        
        home_url = self.live_server_url + reverse('homepage:home')
        
        button = self.selenium.find_element_by_id('button-submit')
        button.click()
        time.sleep(2)
        
        self.assertEquals(
            self.selenium.current_url, home_url
        )
