from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status


# Create your views here.
def home(request):
    
    
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            status = Status(status = form.data['status'])
            status.save()
            return redirect('homepage:home')
    
    else:
        form = StatusForm()
    
    temp = Status.objects.all()
    
    
    return render(request, 'home.html', {'status_list': temp,
                                         'status_form': form })

