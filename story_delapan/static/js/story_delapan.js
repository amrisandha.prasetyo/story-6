$(document).ready(function(){
    var item, title, author, publisher, bookLink, bookImg
    var bookUrl = "https://www.googleapis.com/books/v1/volumes?q=" ;
    var searchData;
    var output_result = $("tbody");
    
    $("tabel").hide();
    
    
    $("#search").click(function(){
        $("tabel").show(1000);
        searchData = $("#search-box").val();
        $("tbody").html("");
        
        if(searchData === "" || searchData === null){
            displayError();
        }
        
        else{
            $.ajax({
                url : bookUrl + searchData,
                dataType: "json",
                success: function(response){
                    if(response.totalItems == 0){
                        alert("No Results.");
                    }
                    else {
                        $("title").animate('margin-top: 10px', 1000);
                        $(".book-list").css("visibility", "visible");
                        displayResults(response);
                        return false;
                    }
                },
                type: 'GET'
                }

            );
        
        }
        $("search-box").val("");
    });
    
    
    $("#search-box").keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
                $("tabel").show(1000);
            searchData = $("#search-box").val();
            $("tbody").html("");

            if(searchData === "" || searchData === null){
                displayError();
            }

            else{
                $.ajax({
                    url : bookUrl + searchData,
                    dataType: "json",
                    success: function(response){
                        if(response.totalItems == 0){
                            alert("No Results.");
                        }
                        else {
                            $("title").animate('margin-top: 10px', 1000);
                            $(".book-list").css("visibility", "visible");
                            displayResults(response);
                            return false;
                        }
                    },
                    type: 'GET'
                    }

                );

            }
            $("search-box").val("");
        }
    });

    
    function displayResults(res){
        for(var i = 0; i < res.items.length; i++){
            item = res.items[i];
            if ('title' in item.volumeInfo == false){
                title = "-";
            }
            else{
                title = item.volumeInfo.title;
            }
            
            if ('authors' in item.volumeInfo == false){
                author = "-";
            }
            else{
                author = item.volumeInfo.authors;
            }
            
            if ('publisher' in item.volumeInfo == false){
                publisher = "-";
            }
            else{
                publisher = item.volumeInfo.publisher;
            }
            
            if ('imageLinks' in item.volumeInfo == false){
                bookImg = "-";
            }
            else{
                bookImg = item.volumeInfo.imageLinks.thumbnail;
            }
            
            output_result.append("<tr>");
            output_result.append("<td>" + "<img src = "  + bookImg + ">"  + "</td>");
            output_result.append("<td>" + title + "</td>");
            output_result.append("<td>" + author + "</td>");
            output_result.append("<td>" + publisher + "</td>");
            output_result.append("</tr>");
        }
    }

});