from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse, resolve

from .views import story_delapan

from django.test import LiveServerTestCase
from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

import time

# Create your tests here.


#Testing HTML
class FormHTMLTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.story_delapan_url = reverse('story_delapan:story_delapan')
        
    def test_text_exist(self):
        response = self.client.get(self.story_delapan_url)
        self.assertIn('Search for Books!', response.content.decode())

    def test_form(self):
        response = self.client.get(self.story_delapan_url)
        self.assertContains(response, '<input')
        

# Testing URLs
class TestUrls(SimpleTestCase):
    def test_home_url_is_resolved(self):
        url = reverse('story_delapan:story_delapan')
        self.assertEquals(resolve(url).func, story_delapan)
        

# Testing Views
class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.story_tujuh_url = reverse('story_delapan:story_delapan')
    
    def test_status_GET(self):
        response = self.client.get(self.story_tujuh_url)
        
        self.assertEquals(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)
        self.assertTemplateUsed(response, 'story_delapan.html', 'base.html')
        
    def test_status_GET_wrong_url(self):
        url = '/kwek'
        response = self.client.get(url)
        
        self.assertEqual(response.status_code, 404)
        

# Funcional Testing
#class TestSelenium(StaticLiveServerTestCase):
#    def setUp(self):
#        super().setUp()
#        chrome_options = webdriver.ChromeOptions()
#        chrome_options.add_argument('--no-sandbox')
#        chrome_options.add_argument('--headless')
#        chrome_options.add_argument('--disable-gpu')
#        self.selenium = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
#
#        #self.browser = webdriver.Chrome(executable_path='D:\KULIAH\PPW\story_enam\homepage\chromedriver.exe')
#
#    
#    def tearDown(self):
#        self.selenium.close()
#    
#    
#    def test_user_sees_page(self):
#        self.selenium.get(self.live_server_url + '/story_tujuh')
#        time.sleep(2)
#        
#        self.assertEquals(
#            self.selenium.find_element_by_tag_name('h1').text, 'Get To Know Me!'
#        )
#        
#        self.assertEquals(
#         self.selenium.find_element_by_tag_name('h3').text, 'Activities'
#        )
#        
##        self.assertEquals(
##         self.selenium.find_element_by_tag_name('h3').text, 'Experience'
##        )
##        
##        self.assertEquals(
##         self.selenium.find_element_by_tag_name('h3').text, 'Achievements'
##        )
#        
        