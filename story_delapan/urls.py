from django.urls import path
from . import views

app_name = 'story_delapan'

urlpatterns = [
    path('', views.story_delapan, name='story_delapan'),
]