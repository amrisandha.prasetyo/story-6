from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse, resolve


from django.test import LiveServerTestCase
from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

import time

# Create your tests here.


#Testing HTML
class FormHTMLTest(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_login_text_exist(self):
        response = self.client.get('/story_sembilan/login/')
        self.assertIn('Login', response.content.decode())
        self.assertIn('Username:', response.content.decode())
        self.assertIn('Password:', response.content.decode())

    def test_login_form(self):
        response = self.client.get('/story_sembilan/login/')
        self.assertContains(response, '<input')
        self.assertContains(response, '<button')
        
        
## Testing URLs
#class TestUrls(SimpleTestCase):
#    def test_home_url_is_resolved(self):
#        url = reverse('story_sembilan:login')
#        self.assertEquals(resolve(url).func, story_sembilan)
#         

# Testing Views
class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_status_GET(self):
        
        response = self.client.get('/story_sembilan/login/')
        self.assertEquals(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)
        self.assertTemplateUsed(response, 'registration/login.html', 'base.html')
        
    def test_status_GET_wrong_url(self):
        url = '/kwek'
        response = self.client.get(url)
        
        self.assertEqual(response.status_code, 404)
        

# Funcional Testing
class TestSelenium(StaticLiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')

        #self.browser = webdriver.Chrome(executable_path='D:\KULIAH\PPW\story_enam\homepage\chromedriver.exe')

    
    def tearDown(self):
        self.selenium.close()
    
    
    def test_user_sees_page(self):
        self.selenium.get(self.live_server_url + '/story_sembilan/login/')
        time.sleep(2)
        
        self.assertEquals(
            self.selenium.find_element_by_tag_name('h2').text, 'Login'
        )
        
        