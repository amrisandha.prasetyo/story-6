from django.urls import path
from . import views

app_name = 'story_sembilan'

urlpatterns = [
    path('', views.story_sembilan, name='story_sembilan'),
]