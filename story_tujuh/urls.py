from django.urls import path
from . import views

app_name = 'story_tujuh'

urlpatterns = [
    path('', views.story_tujuh, name='story_tujuh'),
]